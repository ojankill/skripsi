# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#


$(document).ready ->
  $("#user-tables").dataTable
    bProcessing: true
    bServerSide: true
    sAjaxSource: $('#user-tables').data('url')
    sPaginationType: "bootstrap"
    aoColumnDefs: [
      bSortable: false,
      aTargets: [4]
    ]
