class StaticPageController < ApplicationController
  def home
    @journals = Journal.has_online_accounts
  end
end
