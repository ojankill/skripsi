class Admin::UserController < ApplicationController
  before_action :admin_user!
  before_action :set_user, only: [:edit, :update, :destroy]
  before_action :remove_unused_params, only: :update

  def index
    respond_to do |format|
      format.html
      format.json {render json: UserDatatables.new(view_context)}
    end
  end

  def new
    @user = User.new
    respond_to do |format|
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        flash[:notice] = "User was successfuly created"
        format.js
      else
        format.js {render :new}
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        flash[:notice] = "User was successfuly updated"
        format.js
      else
        format.js {render :edit}
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_user_index_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end


  def user_params
    params.require(:user).permit(:full_name, :email, :role, :password, :password_confirmation)
  end

  def remove_unused_params
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password_confirmation].blank?
  end
end
