class JournalsController < ApplicationController

  def access
    journal = Journal.find(params[:id])
    create_instance journal
    redirect_to proxy_root_url(subdomain: journal.request_domain)
  end
end
