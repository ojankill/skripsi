worker_processes 2

working_directory "/Volumes/Projects/Ruby/rails/skripsi"

listen "/tmp/unicorn.journalist.sock", backlog: 64

pid "./tmp/pids/unicorn.pid"

stderr_path "./log/#{ENV['RAILS_ENV']}.log"

stdout_path "./log/#{ENV['RAILS_ENV']}.log"

timeout 30

preload_app true

GC.respond_to?(:copy_on_write_friendly=) and
  GC.copy_on_write_friendly = true

check_client_connection false

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end
