require 'sidekiq/web'
require 'sidetiq/web'

class ProxySubdomain
  def self.matches? req
    req.subdomain.present? && req.subdomain != "www"
  end
end
class NoSubdomain
  def self.matches? req
    !req.subdomain.present? || (req.subdomain.present? && req.subdomain == "www")
  end
end
Rails.application.routes.draw do

  devise_for :users, skip: [:registrations]
  devise_scope :user do
    unauthenticated do
      get "/" =>  "devise/sessions#new"
    end
    authenticated :user do
      get '/users/edit' => 'devise/registrations#edit', as: 'edit_user_registration'
      put '/users/:id' => 'devise/registrations#update', as: 'registration'
    end
  end

  authenticated :user do
    constraints(ProxySubdomain) do
      mount Journalist::Middleman::Server, at: '/', as: 'proxy_root'
    end
  end

  constraints(NoSubdomain) do
    authenticated :user, lambda{|u| u.role == "ADMIN"} do
      mount Sidekiq::Web => "/sidekiq"
      namespace :admin do
        with_options except: :show do |t|
          t.resources :journals do
            t.resources :journal_accounts do
              with_options action: "set_status" do |map|
                map.post '/enable', action: "set_status"
                map.post '/disable', action: "set_status"
              end
            end
          end
          t.resources :user, except: :show
        end
      end
    end
    root to: "static_page#home"
    get '/journals/:id/access' => "journals#access", as: 'access_journal'
  end
end
